﻿// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Company:............... J.H. Kelly
// Department:............ BIM/VC
// Website:............... http://www.jhkelly.com
// Repository:............ https://github.com/jhkweb/Sandbox
// Solution:.............. Sandbox
// File:.................. Program.cs ✓✓
// Last Edited By:........ Chris Hildebran
// Application Diagram:... https://lucid.app/lucidchart/60c44a1c-6fe6-4d3a-88df-d2bac529a4d9/edit
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace TestingDotNetFramework;

using System.Diagnostics;

internal class Program
{

	private static void Main(string[] args)
	{
		WriteDailyNotes();
	}


	private static void WriteDailyNotes()
	{
		var sw = new Stopwatch();
		sw.Start();

		var dailyNote = new DailyNote();

		var startDate = new DateTime(1967, 9, 6);

		var currentDate = new DateTime(startDate.Year, startDate.Month, startDate.Day);

		var endDate = new DateTime(2023, 4, 22);

		var sb = new StringBuilder();

		do
		{
			var title = currentDate.Year + "." + currentDate.Month.ToString("00") + "." + currentDate.Day.ToString("00") + " - Daily Note";

			sb.AppendLine($"# {title}");
			sb.AppendLine("#daily-jots #MadeWithSandbox");

			switch (currentDate.Month)
			{
				case 12 when currentDate.Day == 25:

					sb.AppendLine("## Christmas Day");

					break;

				case 1 when currentDate.Day == 1:

					sb.AppendLine("## New Year's Day");

					break;

				case 9 when currentDate.Day == 6:

					sb.AppendLine("## Chris Hildebran's Birthday");

					break;

				case 10 when currentDate is { Day: 4, Year: >= 1972 }:

					sb.AppendLine("## Christi Hildebran's Birthday");

					break;

				case 7 when currentDate.Day == 2:

					sb.AppendLine("## David Henry Hildebran's Birthday");

					break;
			}

			dailyNote.WriteToFile(title, sb.ToString());

			currentDate = currentDate.AddDays(1);

			sb.Clear();
		}
		while (currentDate != endDate);

		sw.Stop();

		Console.WriteLine(sw.Elapsed.ToString());
		Console.Read();
	}

}