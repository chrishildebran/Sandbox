﻿namespace TestingDotNetFramework;

internal class MathSandbox
{

	public static void RoundTest()
	{
		var numbers = new List<double>
		{
			1.25,
			1.49,
			1.5,
			1.51,
			1.75
		};

		foreach (var number in numbers) Console.WriteLine(Math.Round(number));

		Console.Read();
	}
}