﻿// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Company:............... J.H. Kelly
// Department:............ BIM/VC
// Website:............... http://www.jhkelly.com
// Repository:............ https://github.com/jhkweb/Sandbox
// Solution:.............. Sandbox
// File:.................. DailyNote.cs ✓✓
// Last Edited By:........ Chris Hildebran
// Application Diagram:... https://lucid.app/lucidchart/60c44a1c-6fe6-4d3a-88df-d2bac529a4d9/edit
// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

namespace DotNetCore;

internal class DailyNote
{

	public void WriteToFile(string filename, string lineToWrite)
	{
		var path     = "C:\\DailyNoteOutput\\DotNeCore\\";

		var directoryInfo = new DirectoryInfo(path);

		var attempts = 0;

		while (!directoryInfo.Exists && attempts < 3)
		{
			directoryInfo.Create();

			attempts++;
		}

		if (attempts >= 3)
		{
			return;
		}

		var filePath = $"{directoryInfo.FullName}\\{filename}.md";

		if (File.Exists(filePath))
		{
			var lines = File.ReadAllLines(filePath);

			if (lines.Contains(lineToWrite))
			{
				return;
			}
		}

		using var streamWriter = File.AppendText(filePath);

		streamWriter.WriteLine(lineToWrite);
	}

}