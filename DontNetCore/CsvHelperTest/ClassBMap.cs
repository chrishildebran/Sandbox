﻿namespace DotNetCore.CsvHelperTest;

using CsvHelper.Configuration;

public class ClassBMap : ClassMap<ClassB>
{

	private ClassBMap()
	{
		Map(m => m.PropertyE).Name("ClassB.PropertyE");

		Map(m => m.PropertyD).Name("ClassB.PropertyD");
	}
}

public class ClassAMap : ClassMap<ClassA>
{

	private ClassAMap()
	{
		Map(m => m.Name).Name("Name");

		Map(m => m.Id).Name("Id");
	}
}